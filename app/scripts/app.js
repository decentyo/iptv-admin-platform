'use strict';

/**
 * @ngdoc overview
 * @name iptvPlatformApp
 * @description
 * # iptvPlatformApp
 *
 * Main module of the application.
 */
angular
  .module('iptvPlatformApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngTable'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl',
        controllerAs: 'users'
      })
      .when('/users/:id', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl',
        controllerAs: 'user'
      })
      .when('/packages', {
        templateUrl: 'views/packages.html',
        controller: 'PackagesCtrl',
        controllerAs: 'packages'
      })
      .when('/packages', {
        templateUrl: 'views/packages.html',
        controller: 'PackagesCtrl',
        controllerAs: 'packages'
      })
      .when('/packages/:id', {
        templateUrl: 'views/package.html',
        controller: 'PackageCtrl',
        controllerAs: 'package'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
