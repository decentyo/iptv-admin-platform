'use strict';

/**
 * @ngdoc service
 * @name iptvPlatformApp.packages
 * @description
 * # packages
 * Service in the iptvPlatformApp.
 */
angular.module('iptvPlatformApp')
  .service('packages', function ($resource) {
    return $resource('http://localhost:3000/packages/:id', null, {
      update: {method: 'PUT'}
    });
  });
