'use strict';

/**
 * @ngdoc service
 * @name iptvPlatformApp.users
 * @description
 * # users
 * Service in the iptvPlatformApp.
 */
angular.module('iptvPlatformApp')
  .factory('users', function ($resource) {
    return $resource('http://localhost:3000/users/:id', null, {
      update: {method: 'PUT'}
    });
  });
