'use strict';

/**
 * @ngdoc function
 * @name iptvPlatformApp.controller:PackagesCtrl
 * @description
 * # PackagesCtrl
 * Controller of the iptvPlatformApp
 */
angular.module('iptvPlatformApp')
  .controller('PackagesCtrl', function ($scope, packages) {
    packages.query(function (data) {
      $scope.packages = data;
    });
  });
