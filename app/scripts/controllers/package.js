'use strict';

/**
 * @ngdoc function
 * @name iptvPlatformApp.controller:PackageCtrl
 * @description
 * # PackageCtrl
 * Controller of the iptvPlatformApp
 */
angular.module('iptvPlatformApp')
  .controller('PackageCtrl', function ($scope, $routeParams, $resource, packages) {
    packages.get({id: $routeParams.id}, function (packageData) {
      $scope.package = packageData;

      // TODO: should be handled by the server
      $resource('http://localhost:3000/packages/:id/channels', {id: '@id'}).query({id: $routeParams.id}, function (channelsData) {
        $scope.channels = channelsData;
      });

      $resource('http://localhost:3000/packages/:id/users', {id: '@id'}).query({id: $routeParams.id}, function (usersData) {
        $scope.users = usersData;
      });
    });
  });
