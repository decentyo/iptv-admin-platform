'use strict';

/**
 * @ngdoc function
 * @name iptvPlatformApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the iptvPlatformApp
 */
angular.module('iptvPlatformApp')
  .controller('UsersCtrl', function ($scope, $resource, users) {
    users.query(function (data) {
      $scope.users = data;
    });
  });
