'use strict';

/**
 * @ngdoc function
 * @name iptvPlatformApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the iptvPlatformApp
 */
angular.module('iptvPlatformApp')
  .controller('UserCtrl', function ($scope, $routeParams, users, packages) {
    users.get({id: $routeParams.id}, function (userData) {
      $scope.user = userData;

      // TODO: should be handled by the server
      packages.get({id: userData.packageId}, function (packageData) {
        $scope.package = packageData;
      })
    });

    $scope.submit = function () {
      users.update({id: $routeParams.id}, {name: $scope.user.name, mail: $scope.user.mail});
    };
  });
