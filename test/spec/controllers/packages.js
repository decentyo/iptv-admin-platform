'use strict';

describe('Controller: PackagesCtrl', function () {

  // load the controller's module
  beforeEach(module('iptvPlatformApp'));

  var PackagesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PackagesCtrl = $controller('PackagesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PackagesCtrl.awesomeThings.length).toBe(3);
  });
});
